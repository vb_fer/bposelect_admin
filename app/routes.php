<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('login',array('as' => 'login' , function() {
	return View::make('login');
}))->before('guest');

Route::post('login',array('uses' => 'HomeController@doLogin'));

Route::get('/', function()
{
	return View::make('index');
})->before('auth');

Route::get('logout', array('uses' => 'HomeController@doLogout'));

Route::group(array('before'=>'auth'), function() {  
	Route::resource('companies','CompanyListController');
	
	Route::get('services',function()
	{ 
		$services = Service::orderby('name')->paginate(10);
		return View::make('services')->with('services',$services);
	});
	
	Route::resource('location','LocationController');
	
	Route::resource('ads','AdsController');
	
	Route::get('features',function()
	{ 
		$features = Feature::orderby('name')->paginate(5);
		return View::make('features')->with('features',$features);
	});
	
	Route::get('seatpackage',function()
	{ 
		$packages = SeatPackage::orderby('name')->paginate(10);
		return View::make('seatpackage')->with('packages',$packages);
	});
	
	Route::get('sortlistings',['uses'=>'AdsController@sortListing']);
	
	
	Route::get('companyaddresses/{id}',['uses'=>'CompanyListController@editcompanyaddress']);
	Route::post('companyaddress','CompanyListController@saveAddress');
	Route::delete('companyaddress/{id}',['uses'=>'CompanyListController@deleteAddress','as' => 'companyaddress.destroy']);



	Route::get('priorityads/{id}',['uses'=>'CompanyListController@addtoPrioritylisting']);
	Route::delete('priority/{id}',['uses'=>'CompanyListController@removePrioritylisting']);
	
});

Route::post('companies/search','CompanyListController@searchCompany');

//start services routes



Route::post('services','ServicesController@saveService');

Route::put('services','ServicesController@updateService');

Route::delete('services/{id}',['uses'=>'ServicesController@deleteService','as' => 'services.destroy']);
//end services routes


//start location routes







//end location routes

//start features routes



Route::post('features','FeaturesController@saveFeature');

Route::put('features','FeaturesController@updateFeature');

Route::delete('features/{id}',['uses'=>'FeaturesController@deleteFeature','as' => 'feature.destroy']);

// end feature routes
/*
Route::get('seatmodel',function()
{ 
	return View::make('seatmodel');
});
*/
//start seatpackage routes



Route::post('seatpackage','SeatPackageController@savePackage');

Route::put('seatpackage','SeatPackageController@updatePackage');

Route::delete('seatpackage/{id}',['uses'=>'SeatPackageController@deletePackage','as' => 'package.destroy']);

//end seat package routes

