<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBanneradsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bannerads', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('image');
			$table->string('page');
			$table->string('location');
			$table->string('bannerurl');
			$table->integer('bannerclicks');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bannerads');
	}

}
