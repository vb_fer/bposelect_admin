<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeatModelPackageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('SeatModel_Package', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('companyID');
			$table->integer('packageID');
			$table->integer('seatmodelID');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('SeatModel_Package');
	}

}
