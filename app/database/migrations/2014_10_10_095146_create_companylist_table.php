<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanylistTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('companylist', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->longText('description');
			$table->longText('shortdescription');
			$table->boolean('aumanaged');
			$table->string('contactperson');
			$table->string('mobile');
			$table->string('phone');
			$table->string('fax');
			$table->string('email');
			$table->string('headoffice');
			$table->string('website');
			$table->string('smtwitter');
			$table->string('smfacebook');
			$table->string('smlinkedin');
			$table->string('smgooglecircle');
			$table->string('smyoutube');
			$table->string('smblog');
			$table->string('smother');
			$table->string('employeecount');
			//$table->boolean('stickysearch');
			$table->string('bannerfilename');
			$table->string('galleryshortcode');
			
			
			
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('companylist');
	}

}
