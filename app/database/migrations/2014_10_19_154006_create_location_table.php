<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('location', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->integer('isparent');
			$table->longText('locationdesc');
			$table->float('gmapsLatitude');
			$table->float('gmapsLongtitude');
			$table->string('nearesttransport');
			$table->string('distanceairport');
			$table->string('topattractions');
			$table->string('galleryshortcode');
			$table->string('headerimage');
			$table->integer('clickviews');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('location');
	}

}
