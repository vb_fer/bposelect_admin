<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeatModelCompTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('SeatModel_Comp', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('seatModelID');
			$table->integer('companyID');
			$table->integer('priceRangeMin');
			$table->integer('priceRangeMax');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('SeatModel_Comp');
	}

}
