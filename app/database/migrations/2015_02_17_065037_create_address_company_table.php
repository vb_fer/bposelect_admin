<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressCompanyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('address_company', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('companyID');
			$table->string('address');
			$table->string('othercontactperson');
			$table->string('othercontactnumber');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('address_company');
	}

}
