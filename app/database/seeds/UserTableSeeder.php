<?php

class UserTableSeeder extends Seeder
{

	public function run()
	{
		DB::table('users')->delete();
		User::create(array(
			'name'     => 'Admin',
			'username' => 'admin',
			'email'    => 'vbfc12@gmail.com',
			'password' => Hash::make('admin101'),
		));
	}

}
