<?php

class servicesTableSeeder extends Seeder
{

	public function run()
	{
		DB::table('services')->delete();
		Service::create(array(
			'name'     => 'Administrative Support',
		));
		Service::create(array(
			'name'     => 'Manufacturing',
		));
		Service::create(array(
			'name'     => 'Creative Solutions',
		));
	}

}