<?php

class SeatModelSeeder extends Seeder
{

	public function run()
	{
		DB::table('SeatModel')->delete();
		SeatModel::create(array(
			'name'     => 'Seat Leasing',
		));
		SeatModel::create(array(
			'name'     => 'Fully Managed Services',
		));

	}

}