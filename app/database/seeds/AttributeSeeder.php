<?php

class AttributeSeeder extends Seeder
{

	public function run()
	{
		DB::table('location')->delete();
		DB::table('feature')->delete();
		
		
		Location::create(array(
			'name'     => 'Metro Manila',
			'isparent' => 0
		));
		
		Feature::create(array(
			'name'     => 'Employee Training and Development',
		));
		
	}

}