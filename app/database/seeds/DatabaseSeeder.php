<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		$this->call('CompanyListSeeder');
		$this->call('AttributeSeeder');
		$this->call('SeatPackageSeeder');
		$this->call('UserTableSeeder');
		$this->call('servicesTableSeeder');
		$this->call('SeatModelSeeder');
	}

}
