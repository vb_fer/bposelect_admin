<?php

class CompanyListController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$companylist = CompanyList::orderBy('name')->paginate(10);
		//$companylist = $companylist->paginate(10);
		
		
		
		//echo "<pre>"; print_r($companylist);
		 return View::make('companies.companies')->with('companylist', $companylist);	
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		
		
		$seatmodel = SeatModel::all();
		$location = Location::where("isparent","=",0)->get();
		$location1 = array();
		$location_list = array();
		
		$budgets = array("0-149"=>"Below $149",
						 "150-249"=>"$150 - $249",
						 "250-349"=>"$250 - $349",
						 "350-449"=>"$350 - $449",
						 "450-549"=>"$450 - $549",
						 "550-649"=>"$550 - $649",
						 "650-749"=>"$650 - $749",
						 "750-849"=>"$750 - $849",
						 "850-0"=>"$850 above");
		
		foreach($location as $locCity){
			
			$location2 = Location::where("isparent","=",$locCity->id)->get();
			
			foreach($location2 as $locCity2){
				$location_list[$locCity2->id] = $locCity2->name;
				
			}
		//echo count($location_list);
		
		if(count($location_list) != 0) {
			$location1[$locCity->name] = $location_list;			
		//unset($location1[$locCity->name]);
		}
		
		
			$location_list = null;
		}
		
		//echo "<pre>";
		//print_r($location1);
		//exit();
		
		return View::make('companies.create')->with('seatmodel',$seatmodel)
											 ->with('locationList',$location1)
											 ->with('budgets',$budgets);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
	
		$rules = array(
			'name'			=>	'required',
			'description'	=>	'required',
			'contact_person'=>	'required',
			'email'			=>	'required|email',
			'headoffice'	=>	'required',
			'aumanaged'		=>	'required',
			'seatmodel'		=>	'required',
			'services'		=>	'required',
			'location'		=>	'required'
			
		);
		
		$v = Validator::make(Input::all(),$rules);
		
		if(Input::get('seatmodel') != null){
		foreach(Input::get('seatmodel') as $val){
			
			$rule = array(
				'pricerange' . $val	=>	'required|integer',
				'package' . $val	=>	'required',
			);
			
			$v->sometimes(
					array('pricerange' . $val,'package' . $val),
					'required',
					
					function($input){
						return $input->seatmodel != "";
			});
			
		}
		}
		if($v->fails()){
			return Redirect::to('companies/create')
			->withErrors($v)
			->withInput(Input::all());
		}
		
		else {
		
		
		
		$company = new CompanyList;
			$company->name				=	Input::get('name');
			$company->description		=	Input::get('description');
			$company->aumanaged			=	Input::get('aumanaged');
			$company->shortdescription 	= 	Input::get('shortdescription');
			$company->contactperson 	= 	Input::get('contact_person');
			$company->mobile 			= 	Input::get('mobile');
			$company->phone 			= 	Input::get('phone');
			$company->fax 				= 	Input::get('fax');
			$company->email 			= 	Input::get('email');
			$company->headoffice 		= 	Input::get('headoffice');
			$company->website 			= 	Input::get('website');
			$company->smtwitter 		= 	Input::get('tw');
			$company->smfacebook 		= 	Input::get('fb');
			$company->smlinkedin 		= 	Input::get('ln');
			$company->smgooglecircle 	= 	Input::get('gc');
			$company->smyoutube		 	= 	Input::get('yt');
			$company->smblog		 	= 	Input::get('blog');
			$company->employeecount		= 	Input::get('employeecount');
			//$company->stickysearch		= 	Input::get('stickysearch');
			
			
			if(Input::hasFile('filebanner')){
		$bannerfilename = str_random(12) . "." . Input::file('filebanner')->getClientOriginalExtension();
		
		//Input::file('filebanner')->move(base_path().'../../search/public/banners/',$bannerfilename);
		Input::file('filebanner')->move('/home/webzaps/public_html/bposelect/search/public/banners/',$bannerfilename);
		
			$company->bannerfilename	= 	$bannerfilename;
			}
		$comp = $company->save();
		
		$seatmodel = new SeatModelComp;
		
		foreach(Input::get('seatmodel') as $value){
			//$seatmodel_comp = $company->seatmodel()->attach($value);
				
				$pricerange1 = 'pricerange' . $value;
				
				$prices = explode("-",Input::get($pricerange1));
				
				
				$company->seatmodel()->attach($value,array('priceRangeMin' => $prices[0], 'priceRangeMax' => $prices[1]));
				
				$packageIndex = "package" . $value;
				foreach(Input::get($packageIndex) as $value2){
				
					//$seatmodel->seat_package()->attach($value2);
					$company->seatpackage()->attach($value2,array('seatmodelID' => $value));
					
				}
			
		}
		
		
		
		
		$addressctr = Input::get("addressctr");
		for($x=1;$x<=$addressctr;$x++){
		
			$otheradd = new AddressCompany;
			$otheradd->address = Input::get("otheraddress" . $x);
			$otheradd->othercontactperson = Input::get("othercontact" . $x);
			$otheradd->othercontactnumber = Input::get("othercontactnumber" . $x);
			$otheradd->companyID = $company->id;
			$otheradd->save();
			
			//echo Input::get("otheraddress" . $x);
		}
		
		
		
		foreach(Input::get('services') as $value){
			$company->services()->attach($value);
		}
		
		foreach(Input::get('location') as $value){
			$company->location()->attach($value);
		}
		
		if(Input::has('features')){
		foreach(Input::get('features') as $value){
			$company->feature()->attach($value);
		}
		}
		
		
		
		Session::flash('message', 'Successfully added new company!');
		
		return Redirect::to('companies');
		}
		/*
		foreach (Input::get('seatmodel') as $value) {
			
			//$company->seatmodel()->attach();
			
			//print_r(Input::get('package' . $value));
		}
		*/
		
		
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$company = CompanyList::find($id);
		$seatmodel = SeatModel::all();
		$sm_arr = array();
		$package_arr = array();
		$location = Location::where("isparent","=",0)->get();
		$location1 = array();
		$location_list = array();
		$budgets = array("0-149"=>"Below $149",
						 "150-249"=>"$150 - $249",
						 "250-349"=>"$250 - $349",
						 "350-449"=>"$350 - $449",
						 "450-549"=>"$450 - $549",
						 "550-649"=>"$550 - $649",
						 "650-749"=>"$650 - $749",
						 "750-849"=>"$750 - $849",
						 "850-0"=>"$850 above");
		
		foreach($location as $locCity){
			$location2 = Location::where("isparent","=",$locCity->id)->get();
				foreach($location2 as $locCity2){
					$location_list[$locCity2->id] = $locCity2->name;
				}
		
			if(count($location_list) != 0) {
				$location1[$locCity->name] = $location_list;			
				}
				
			$location_list = null;
		}
		$package_arr1[] = array();
		$package_arr2[] = array();

		foreach($company->seatpackage as $package){
						if($package->pivot->seatmodelID == 1){	
							$package_arr1[] = $package->id;
						}
						
						if($package->pivot->seatmodelID == 2){	
							$package_arr2[] = $package->id;
						}
					}
		
		$package_arr = array(1=>$package_arr1,2=>$package_arr2);
		
		/*
		foreach($company->seatmodel as $seatmodel){
						if($seatmodel->pivot->seatmodelID == 1){	
							$price_arr1[] = $seatmodel->pivot->priceRangeMin . "-" . $seatmodel->pivot->priceRangeMax;
						}
						
						if($seatmodel->pivot->seatmodelID == 1){	
							$price_arr2[] = $seatmodel->pivot->priceRangeMin . "-" . $seatmodel->pivot->priceRangeMax;
						}
					}
		*/
		
		
		
		$price_arr[] = array();
		$price_arr1[] = array();
		$price_arr2[] = array();
		//echo"<pre>";print_r($company->seatmodel->toArray());
		foreach($company->seatmodel as $seatmodel1) {
			
			if($seatmodel1->id == 1){	
				$price_arr1 = $seatmodel1->pivot->priceRangeMin . "-" . $seatmodel1->pivot->priceRangeMax;
					}
						
			if($seatmodel1->id == 2){	
				$price_arr2 = $seatmodel1->pivot->priceRangeMin . "-" . $seatmodel1->pivot->priceRangeMax;
						}
			
		//$price_arr[$seatmodel1->id] = $seatmodel1->pivot->priceRangeMin . "-" . $seatmodel1->pivot->priceRangeMax;
			
		}
		
		$price_arr = array(1=>$price_arr1,2=>$price_arr2);
		//echo"<pre>";print_r($price_arr);exit;
		
		foreach($seatmodel as $key => $sm){
			$sm_arr[$sm->id] = (in_array($sm->id,array_fetch($company->seatmodel->toArray(),'id')))?1:0;
			
		}
		
			return View::make('companies.edit')
							->with('companyInfo',$company)->with('seatmodel',$seatmodel)
							->with('budgets',$budgets)
							->with('locationList',$location1)
							->with('package',$package_arr)
							->with('sm',$sm_arr)
							->with('price',$price_arr);
							
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
	//DB::transaction(function(){	
	
		$rules = array(
			'name'			=>	'required',
			'description'	=>	'required',
			'contact_person'=>	'required',
			'email'			=>	'required|email',
			'headoffice'	=>	'required',
			'aumanaged'		=>	'required',
			'seatmodel1'		=>	'required',
			'services'		=>	'required',
			'location'		=>	'required',
			
		);
		
		$v = Validator::make(Input::all(),$rules);
		
		if(Input::get('seatmodel1') != null){
		foreach(Input::get('seatmodel1') as $val){
			
			$rule = array(
				'pricerange' . $val	=>	'required',
				'package' . $val	=>	'required',
			);
			
			$v->sometimes(
					array('pricerange' . $val,'package' . $val),
					'required',
					
					function($input){
						return $input->seatmodel1 != "";
			});
			
		}
		}
		
		if($v->fails()){
			return Redirect::to('companies/'.$id.'/edit')
			->withErrors($v)
			->withInput(Input::all());
		}
		
		else {
		
		
		
			$company = CompanyList::find($id);
			$company->name				=	Input::get('name');
			$company->description		=	Input::get('description');
			$company->aumanaged			=	Input::get('aumanaged');
			$company->shortdescription 	= 	Input::get('shortdescription');
			$company->contactperson 	= 	Input::get('contact_person');
			$company->mobile 			= 	Input::get('mobile');
			$company->phone 			= 	Input::get('phone');
			$company->fax 				= 	Input::get('fax');
			$company->email 			= 	Input::get('email');
			$company->headoffice 		= 	Input::get('headoffice');
			$company->website 			= 	Input::get('website');
			$company->smtwitter 		= 	Input::get('tw');
			$company->smfacebook 		= 	Input::get('fb');
			$company->smlinkedin 		= 	Input::get('ln');
			$company->smgooglecircle 	= 	Input::get('gc');
			$company->smyoutube		 	= 	Input::get('yt');
			$company->smblog		 	= 	Input::get('blog');
			$company->employeecount		= 	Input::get('employeecount');
			//$company->stickysearch		= 	Input::get('stickysearch');
			$company->galleryshortcode	= 	Input::get('gallerycode');
			
			if(Input::hasFile('filebanner')){
			$bannerfilename = str_random(12) . "." . Input::file('filebanner')->getClientOriginalExtension();
		
			//Input::file('filebanner')->move(base_path().'../../search/public/banners/',$bannerfilename);
			
			Input::file('filebanner')->move('/home/webzaps/public_html/bposelect/search/public/banners/',$bannerfilename);
			
			$company->bannerfilename	= 	$bannerfilename;
			}
		
			
			
		$company->save();
		
		//$seatmodel = new SeatModelComp;
		//echo "<pre>";
		CompanyList::find($id)->seatpackage()->detach();
		foreach(Input::get('seatmodel1') as $value){
			
				
				$pricerangeIndex = 'pricerange' . $value;
			
				$pricerange = explode("-",Input::get($pricerangeIndex));
				
				$pivot_pricerange[$value] = array('priceRangeMin' => $pricerange[0], 
												  'priceRangeMax' => $pricerange[1]);
								
				$packageIndex = "package" . $value;
				
				//print_r(Input::get($packageIndex));
				//$company->seatpackage()->sync(Input::get($packageIndex),array('seatmodelID' => $value)
				foreach(Input::get($packageIndex) as $value2){
				
				
				$company->seatpackage()->attach($value2,array('seatmodelID' => $value));
				
				
				}
				
		}		
		
		
		
		$company->seatmodel()->sync($pivot_pricerange);
		
		
		$company->services()->sync(Input::get('services'));
		
		
		
			$company->location()->sync(Input::get('location'));
		
		
		if(Input::has('features')){
			$company->feature()->sync(Input::get('features'));
		}
		
		//}
		
		Session::flash('message', 'Record Saved');
		
		return Redirect::to('companies');
		}
		
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$company = CompanyList::find($id);
		
		CompanyList::find($id)->location()->detach();
		CompanyList::find($id)->feature()->detach();
		CompanyList::find($id)->services()->detach();
		CompanyList::find($id)->seatmodel()->detach();
		CompanyList::find($id)->seatpackage()->detach();
		
        $company->delete();

        // redirect
        Session::flash('message', 'Company Successfully deleted!');
        return Redirect::to('companies');
	}

	public function searchcompany(){
		
		$searchcomp = CompanyList::where('name','like','%' . Input::get('searchcomp') . '%')->paginate(10);
		//echo count($searchcomp);
		return View::make('companies.search')->with('companylist',$searchcomp);
	}
		
	public function editcompanyaddress($id){
	
		$compaddress = CompanyList::find($id)->addresscompany;
		$companyname = CompanyList::find($id)->name;
		//echo "<pre>";print_r($compaddress->companylist->name);
		return View::make('companies.compaddress')->with("compaddress",$compaddress)
													->with("compid",$id)
													->with("compname",$companyname);
		
	}
	
	public function saveAddress(){
	
		
		
			$rules = array(
			'otheraddress'	=>	'required',
			'othercontact'	=>	'required',
			'othercontactnumber'=>	'required'
			);
	
	
	$id = Input::get("compid");
	
	
		$compaddress = new AddressCompany;
		
		$compaddress->address = Input::get("otheraddress");
		$compaddress->othercontactperson = Input::get("othercontact");
		$compaddress->othercontactnumber = Input::get("othercontactnumber");
		$compaddress->companyID = Input::get("compid");
		
		$compaddress->save();
		  return Redirect::to('companyaddresses/' . $id);
		
		
		//}

}
	public function deleteAddress($id){
	
		$address = AddressCompany::find($id);
		$compid = $address->companyID;
		$address->delete();
		return Redirect::to('companyaddresses/' . $compid);
	}
	
	public function addtoPrioritylisting($id){
	
		$ordersort = DB::table('prioritylist')->max('ordersort');
		$ordersort1 = (empty($ordersort))?0:++$ordersort;
		//echo $ordersort1;
		
		
		
		$priority = new PriorityList;
		
		$priority->companyID = $id;
		$priority->ordersort = $ordersort1;
		
		$priority->save();
		
		return Redirect::to('companies/');
		
	}
	
	public function removePrioritylisting($id) {
		
		$priority1 = PriorityList::find($id);
		$priority1->delete();
		
		return Redirect::to('ads');
		//echo $id;
	
	}
	
}