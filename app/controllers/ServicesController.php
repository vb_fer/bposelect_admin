<?php

class ServicesController extends BaseController {

	public function saveService() {
		
		$service = new Service;
		
		$service->name = Input::get('name');
		
		$service->save();
		
		return Redirect::to('services');
	}	


	public function updateService() {
	
		$service = Service::find(Input::get('id'));
		$service->name = Input::get('name');
		
		$service->save();
		return Redirect::to('services');
	}

	public function deleteService($id) {
	
		$service = Service::find($id);
		
		$service->delete();
				
		return Redirect::to('services');
	}

}