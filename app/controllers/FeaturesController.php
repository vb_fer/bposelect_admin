<?php

class FeaturesController extends BaseController {

	public function saveFeature() {
		
		$feature = new Feature;
		
		$feature->name = Input::get('name');
		
		$feature->save();
		
		return Redirect::to('features');
	}	


	public function updateFeature() {
	
		$feature = Feature::find(Input::get('id'));
		$feature->name = Input::get('name');
		
		$feature->save();
		return Redirect::to('features');
	}

	public function deleteFeature($id) {
	
		$feature = Feature::find($id);
		
		$feature->delete();
				
		return Redirect::to('features');
	}

}