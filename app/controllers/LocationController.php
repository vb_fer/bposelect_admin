<?php

class LocationController extends BaseController {

public function index()
	{
	$location = Location::orderby('name')->paginate(10);
	
	return View::make('location.location')->with('location',$location);
	}

public function create()
	{
		return View::make('location.create');
	
	}

	
public function store()
	{
		$rules = array(
			'name'				=> 'required',
			'locationdesc'		=> 'required',
			'gmapslatitude'		=> 'numeric|required',
			'gmapslongtitude'	=> 'numeric|required'
			);
		
		$valid = Validator::make(Input::all(),$rules);
		
		if($valid->fails()) {
			return Redirect::to('location/create')
				->withErrors($valid)
				->withInput(Input::all());
		
		}
		
		else {
		
			
			$location = new Location;
			
			$location->name		= Input::get('name');
			$location->isparent = Input::get('isparent');
			$location->locationdesc = Input::get('locationdesc');
			$location->gmapsLatitude = Input::get('gmapslatitude');
			$location->gmapsLongtitude = Input::get('gmapslongtitude');
			$location->nearesttransport = Input::get('nearesttransport');
			$location->distanceairport = Input::get('distanceairport');
			$location->topattractions = Input::get('topattractions');
			$location->galleryshortcode = Input::get('gallery');
			
			if(Input::hasFile('filebanner')){
			$bannerfilename = str_random(12) . "." . Input::file('filebanner')->getClientOriginalExtension();
		
			//Input::file('filebanner')->move(base_path().'../../search/public/banners/',$bannerfilename);
			Input::file('filebanner')->move('/home/webzaps/public_html/bposelect/search/public/banners/',$bannerfilename);
			$location->headerimage = $bannerfilename;
			}
			
			
			$location->save();
			
			Session::flash('message','Location saved');
			return Redirect::to('location');
			
		}
		
		
	}
	
public function edit($id)
	{
		//
		$location = Location::find($id);
		
			return View::make('location.edit')
			->with('location',$location);
	}

public function update($id)
	{
		$rules = array(
			'name'				=> 'required',
			'locationdesc'		=> 'required',
			'gmapslatitude'		=> 'numeric|required',
			'gmapslongtitude'	=> 'numeric|required'
			);
		
		$valid = Validator::make(Input::all(),$rules);
		
		if($valid->fails()) {
			return Redirect::to('location/'. $id . '/edit')
				->withErrors($valid)
				->withInput(Input::all());
		
		}
		
		else {
			
		
			$location = Location::find($id);
			$location->name		= Input::get('name');
			$location->isparent = Input::get('parentloc');
			$location->locationdesc = Input::get('locationdesc');
			$location->gmapsLatitude = Input::get('gmapslatitude');
			$location->gmapsLongtitude = Input::get('gmapslongtitude');
			$location->nearesttransport = Input::get('nearesttransport');
			$location->distanceairport = Input::get('distanceairport');
			$location->topattractions = Input::get('topattractions');
			$location->galleryshortcode = Input::get('gallery');
			
			if(Input::hasFile('filebanner')){
			$bannerfilename = str_random(12) . "." . Input::file('filebanner')->getClientOriginalExtension();
		
			//Input::file('filebanner')->move(base_path().'../../search/public/banners/',$bannerfilename);
			
			Input::file('filebanner')->move('/home/webzaps/public_html/bposelect/search/public/banners/',$bannerfilename);
			
			$location->headerimage = $bannerfilename;
			}
			
			$location->save();
			
			Session::flash('message','Location saved');
			return Redirect::to('location');
			
		}
		
	
	}
	
	
public function saveLocation() {
		
		$location = new Location;
		
		$location->name = Input::get('name');
		$location->isparent = Input::get('isparent');
		$location->locationdesc = Input::get('locationdesc');
		$location->gmapsLatitude = Input::get('gmapslatitude');
		$location->gmapsLongtitude = Input::get('gmapslongtitude');
		$location->save();
		
		return Redirect::to('location');
	}	




	public function destroy($id) {
	
		$location = Location::find($id);
		
		$location->delete();
				
		return Redirect::to('location');
	}

}




?>


