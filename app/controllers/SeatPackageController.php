<?php

class SeatPackageController extends BaseController {

public function savePackage() {
		
		$package = new SeatPackage;
		
		$package->name = Input::get('name');
		
		$package->save();
		
		return Redirect::to('seatpackage');
	}	


	public function updatePackage() {
	
		$package = SeatPackage::find(Input::get('id'));
		$package->name = Input::get('name');
		
		$package->save();
		return Redirect::to('seatpackage');
	}

	public function deletePackage($id) {
	
		$package = SeatPackage::find($id);
		
		$package->delete();
				
		return Redirect::to('seatpackage');
	}

}


?>