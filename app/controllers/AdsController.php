<?php

class AdsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$bannerpages = array("home"=>"Home Page",
							 "about"=>"About Us",
							 "outsourcinginfo"=>"Outsourcing Information",
							 "contactus"=>"Contact Us",
							 "glossary"=>"Glossary Page",
							 "location"=>"Locations Page",
							 "search"=>"Search Page",
							 "articles"=>"Popular Articles"
							);
							  
		$bannerlocation = array("top"=>"Top",
								"mid"=>"Middle",
								"bottom"=>"Bottom"
							  );
		$banneradlist = BannerAds::orderBy("page")->get();
		//$sponsored_list = CompanyList::where("stickysearch",1)->get();
		$sponsored_list = PriorityList::with('companylist')->orderby('ordersort','ASC')->get();
		
		//echo"<pre>";print_r($sponsored_list);exit;
		return View::make('ads.ads')->with("sList",$sponsored_list)
									->with("bannerpages",$bannerpages)
									->with("bannerloc",$bannerlocation)
									->with("bannerlist",$banneradlist);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$rules = array(
			'filebanner'	=>	'required',
			'page'			=>	'required',
			'bannerloc'		=>	'required',
			'bannerurl'		=>	'sometimes|url'
		);
		
		$messages = array(
			'required' => 'The :attribute field is required',
			'bannerurl.url' => 'URL is invalid please inlclude http://',
			);
		
		$v = Validator::make(Input::all(),$rules,$messages);
		if($v->fails()){
			
			return Redirect::to('ads/')
			->withErrors($v)
			->withInput(Input::except('filebanner'));
		}
		
		else{
			$bannerads = new BannerAds;
			
			//if(Input::has('filebanner')){
			
			$bannerfilename = str_random(12) . "." . Input::file('filebanner')->getClientOriginalExtension();
		
			//Input::file('filebanner')->move(base_path().'../../search/public/bannersads/',$bannerfilename);
			Input::file('filebanner')->move('/home/webzaps/public_html/bposelect/search/public/bannersads/',$bannerfilename);
			$bannerads->image = $bannerfilename;
			//}
			
			
			$bannerads->page = Input::get('page');
			$bannerads->location = Input::get('bannerloc');
			$bannerads->bannerurl = Input::get('bannerurl');
			
			$bannerads->save();
			
			return Redirect::to('ads/');
		}
	
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$banner = BannerAds::find($id);
		$banner->delete();
		
		File::delete(base_path().'../../search/public/bannersads/' . $banner->image);

        // redirect
        Session::flash('message', 'Banner was Successfully deleted!');
        return Redirect::to('ads');
	}
	
	public function sortListing(){
		
		foreach(Input::get('item') as $key => $val){
				$priority = PriorityList::find($val);
				
				$priority->ordersort = $key + 1;
				$priority->save();
				
				
		}
		
	}

}
