@extends('layouts.layout')



@section('content')
	<h4>Priority listings</h4>
	
	<ul id="sortablelist">
	@foreach($sList as $slist)
	<li id=item-{{$slist->id}}>{{$slist->companylist->name}}	
	{{ Form::open(array('url' => 'priority/' . $slist->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Remove', array('class' => 'btn btn-danger pull-right','style'=>'font-size:12px;padding:1px;')) }}
                {{ Form::close() }}</li>
	@endforeach
</ul>
	
<hr style="margin:60px 0px">

	<fieldset>
	<legend>Banner Ads</legend>
	{{ HTML::ul($errors->all() )}}
	
	{{Form::open(array('url'=>'ads','files'=>true))}}
	
	<div class="col-md-3 margin-bottom-15">
		{{ Form::label('filebanner','Upload Banner',array('class'=>'control-label'))}}
	</div>
	
	<div class="col-md-9 margin-bottom-30">
		{{ Form::file('filebanner','', array('class'=>'form-control'))}}
	</div>
	
	<div class="col-md-3 margin-bottom-15">
		{{ Form::label('page','Page',array('class'=>'control-label'))}}
	</div>
	
	<div class="col-md-9 margin-bottom-30">
		{{Form::select('page',$bannerpages,'',array('class'=>'form-control'))}}
		
	</div>
	
	<div class="col-md-3 margin-bottom-15">
		{{ Form::label('bannerloc','Location',array('class'=>'control-label'))}}
	</div>
	
	<div class="col-md-9 margin-bottom-30">
		{{Form::select('bannerloc',$bannerloc,'',array('class'=>'form-control'))}}
		
	</div>
	<div class="col-md-3 margin-bottom-15">
		{{ Form::label('bannerurl','URL redirect',array('class'=>'control-label'))}}
	</div>
	
	<div class="col-md-9 margin-bottom-30">
		{{Form::text('bannerurl',Input::old('bannerurl'),array('class'=>'form-control'))}}
		
	</div>
	
	
	<div class="col-md-12 margin-bottom-30" style="text-align:center">
	 {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
	 </div>
	 
	
	{{Form::close()}}
	
	<table class="table table-striped table-hover table-bordered">
	<tr>
	<td>Image</td>
	<td>Page</td>
	<td>Location</td>
	<td>URL</td>
	<td>Clicks</td>
	<td></td>
	</tr>
	
	@foreach($bannerlist as $banner)
	
	<tr>
	<td><img src="../../search/public/bannersads/{{$banner->image}}" height="30px"/></td>
	<td>{{$banner->page}}</td>
	<td>{{$banner->location}}</td>
	<td>{{$banner->bannerurl}}</td>
	<td>{{$banner->bannerclicks}}</td>
	<td align="center">
	{{ Form::open(array('url' => 'ads/' . $banner->id, 'class' => '')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
	</td>
	</tr>
	
	@endforeach
	
	
	</table>
	
	
	</fieldset>
	
	
@stop