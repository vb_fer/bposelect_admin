<div class="navbar-collapse collapse templatemo-sidebar">
        
		<ul class="templatemo-sidebar-menu">
          <li>
            {{ Form::open(array('url' => 'companies/search', 'class' => 'navbar-form')) }}
			{{ Form::text('searchcomp',Input::old('searchcomp'), array('class'=>'form-control','id'=>'templatemo_search_box','placeholder'=>'Search Company...'))}}
              
              <input type="submit" value="Go" class="btn btn-default"/>
            {{Form::close()}}
          </li>
          <li class="active"><a href="{{ URL::to('/') }}"><i class="fa fa-home"></i>Dashboard</a></li>
          <li class="sub open">
            <a href="javascript:;">
              <i class="fa fa-database"></i> Company Management <div class="pull-right"><span class="caret"></span></div>
            </a>
            <ul class="templatemo-submenu">
              <li><a href="{{ URL::to('companies') }}">Companies</a></li>
              <li><a href="{{ URL::to('ads') }}">Sponsored List / Banner ads</a></li>
      
            </ul>
          </li>

			<li class="sub open">
            <a href="javascript:;">
              <i class="fa fa-database"></i> Attributes <div class="pull-right"><span class="caret"></span></div>
            </a>
            <ul class="templatemo-submenu">
              <li><a href="{{URL::to('services')}}">Services</a></li>
              <li><a href="{{URL::to('location')}}">Location</a></li>
			  <li><a href="{{URL::to('features')}}">Features</a></li>
			 <!-- <li><a href="{{URL::to('seatmodel')}}">Seat Model</a></li>-->
			  <li><a href="{{URL::to('seatpackage')}}">Seat Packages</a></li>
            </ul>
          </li>

          
          
          <li><a href="preferences.html"><i class="fa fa-cog"></i>Preferences</a></li>
          <li><a href="{{URL::to('logout')}}" ><i class="fa fa-sign-out"></i>Sign Out</a></li>
        </ul> 
      </div>