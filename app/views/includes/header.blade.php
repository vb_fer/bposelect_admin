<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
  <title>Admin</title>
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <meta name="viewport" content="width=device-width">        
  <script src="{{URL::asset('assets/js/jquery.min.js')}}"></script>
  
  <script src="{{URL::asset('assets/js/ckeditor/ckeditor.js')}}"></script>
  <link rel="stylesheet" href="{{URL::asset('assets/css/templatemo_main.css')}}">
  <link rel="stylesheet" href="{{URL::asset('assets/css/multiple-select.css')}}">
   <script>
  $(function() {
  
    $( "#sortablelist" ).sortable({
		axis:'y',
		update: function (event,ui){
			
			var data = $(this).sortable('serialize');
			
			$.ajax({
            data: data,
            type: 'get',
            url: "{{URL::to('sortlistings')}}"
        });
		}
	
	});
    
  });
  </script>
</head>
<body>