@extends('layouts.layout')

@section('content')
	<h1>LOCATION</h1>
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
	@endif
	<h4><a href="{{ URL::to('location/create') }}" class="btn btn-default">Add New</a></h4>	
	<div class="col-md-12">
	<div class="pull-right">{{$location->links()}}</div>
<div class="table-responsive">
              
				
		<table class="table table-striped table-hover table-bordered">
                  <thead>
                    <tr>
                      <!--<th>#</th>-->
                      <th>Location</th>
					  <th>Parent</th>
					<th>action</th>
					
                    </tr>
                  </thead>
                  <tbody>
                    
					@foreach($location as $location)
		
					<tr>
                      <!--<td>{{ $location->id }}</td>-->
                      <td>{{ $location->name }}</td>
					  <td> @if($location->isparent != 0)
								{{ $location->find($location->isparent)->name }}
								@endif
								</td>
							
                      
                      <td><a style="float:left;margin-right:10px" class="btn btn-default" href="{{ URL::to('location/' . $location->id . '/edit') }}" >Edit</a>
						
						{{ Form::open(array('method' => 'DELETE', 'route' =>array('location.destroy', $location->id))) }}
						{{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
						{{ Form::close() }}
					  </td>                    
                      
                      
                    </tr>
                  @endforeach
                  </tbody>
                </table>
		</div>
	</div>	
		 <!-- Modal -->
   
	  <script type="text/javascript">
		function triggerModal(avalue,idvalue) {
 
			document.getElementById('locationname').value = avalue;
			document.getElementById('locationid').value = idvalue;
 
			$('#confirmModal').modal();
 
}
</script>
<script>
		CKEDITOR.replace('locationdesc');
	</script>

@stop