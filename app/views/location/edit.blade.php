@extends('layouts.layout')

@section('content')
<h1>Edit location: {{$location->name}}</h1>
{{ Form::model($location, array('route' => array('location.update', $location->id), 'method' => 'PUT','files'=>true)) }}

<fieldset>
	<legend>Location Information</legend>
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('name','Name',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('name',$location->name, array('class'=>'form-control'))}}
	</div>
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('parent','Parent',array('class'=>'control-label'))}}
	</div>
	
	<?php 
				$locationlist = ['0'=>'Parent'] + Location::lists('name','id');
				//array_unshift($locationlist,"Parent");
				
				?>
	
	<div class="col-md-9 margin-bottom-30">
		{{Form::select('parentloc',$locationlist,$location->isparent,array('class'=>'form-control'))}}
	</div>

	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('description','Description',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::textarea('locationdesc',$location->locationdesc, array('class'=>'form-control','rows'=>'10'))}}
	</div>
	
	
	
</fieldset>

<fieldset>
	<legend>Google Maps Coordinates</legend>
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('latitude','Latitude',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('gmapslatitude',$location->gmapsLatitude, array('class'=>'form-control','placeholder'=>'latitude'))}}
	</div>
	
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('longtitude','Longtitude',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('gmapslongtitude',$location->gmapsLongtitude, array('class'=>'form-control','placeholder'=>'longtitude'))}}
	</div>
	
</fieldset>

<fieldset>
	<legend>Landmarks</legend>
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('nearesttransport','Nearest Transport',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('nearesttransport',$location->nearesttransport, array('class'=>'form-control'))}}
	</div>
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('distanceairport','Distance to Airport',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('distanceairport',$location->distanceairport, array('class'=>'form-control'))}}
	</div>
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('topattractions','Top Attractions',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('topattractions',$location->topattractions, array('class'=>'form-control'))}}
	</div>

	</fieldset>
	
<fieldset>
	<legend>Images and Banner</legend>
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('gallery','Gallery Plugin Short Code',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('gallery',$location->galleryshortcode, array('class'=>'form-control'))}}
	</div>

	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('filebanner','Location Banner',array('class'=>'control-label'))}}
	</div>
	
	<div class="col-md-9 margin-bottom-30">
		{{ Form::label('filebanner','upload Location banner',array('class'=>'control-label'))}}
		{{ Form::file('filebanner','', array('class'=>'form-control'))}}
	</div>
	
</fieldset>
	
<div class="margin-bottom-30">
	 {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
	 </div>
{{ Form::close() }}
<script>
		CKEDITOR.replace('locationdesc');
	</script>

@stop