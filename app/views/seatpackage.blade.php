@extends('layouts.layout')

@section('content')
	<h1>SEAT PACKAGES</h1>
		<div class="table-responsive">
                <h4 class="pull-left">
				{{Form::open(array('url'=>'seatpackage'))}}
				{{ Form::text('name',Input::old('name'), array('class'=>'form-control','placeholder'=>'Seat Package'))}}
				{{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
				{{Form::close()}}
				</h4>
				<div class="pull-right">{{$packages->links()}}</div>
		<table class="table table-striped table-hover table-bordered">
                  <thead>
                    <tr>
                      <!--<th>#</th>-->
                      <th>Package</th>
					<th>action</th>
					
                    </tr>
                  </thead>
                  <tbody>
                    
					@foreach($packages as $packages)
					<tr>
                      <!--<td>{{ $packages->id }}</td>-->
                      <td>{{ $packages->name }}</td>
                      
                      <td><a href="#"  style="float:left;margin-right:10px" class="btn btn-default" onClick='triggerModal("{{$packages->name}}","{{$packages->id}}")'>Edit</a>
						
						{{ Form::open(array('method' => 'DELETE', 'route' =>array('package.destroy', $packages->id))) }}
						{{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
						{{ Form::close() }}
					  </td>                    
                      
                      
                    </tr>
                  @endforeach
                  </tbody>
                </table>
		</div>
		
		 <!-- Modal -->
      <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              
			  
			  {{Form::open(array('url'=>'seatpackage','method'=>'PUT'))}}
				{{ Form::hidden('id',"$packages->id", array('id'=>"packagesid")) }}
				{{ Form::text('name',Input::old("$packages->name"), array('class'=>'form-control','placeholder'=>'Package name','id'=>"packagesname"))}}
				<br>
				{{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
				{{Form::close()}}
            </div>
            <div class="modal-footer">
              
            </div>
          </div>
        </div>
      </div>
	  <script type="text/javascript">
		function triggerModal(avalue,idvalue) {
 
			document.getElementById('packagesname').value = avalue;
			document.getElementById('packagesid').value = idvalue;
 
			$('#confirmModal').modal();
 
}
</script>
@stop