@extends('layouts.layout')

@section('content')
	<h1>SERVICES</h1>
	
	<div class="table-responsive">
                <h4 class="pull-left">
				{{Form::open(array('url'=>'services'))}}
				{{ Form::text('name',Input::old('name'), array('class'=>'form-control','placeholder'=>'Service name'))}}
				{{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
				{{Form::close()}}
				</h4>
				<div class="pull-right">{{$services->links()}}</div>
                <table class="table table-striped table-hover table-bordered">
                  <thead>
                    <tr>
                      <!--<th>#</th>-->
                      <th>Service</th>
					<th>action</th>
					
                    </tr>
                  </thead>
                  <tbody>
                    
					@foreach($services as $services)
					<tr>
                      <!--<td>{{ $services->id }}</td>-->
                      <td>{{ $services->name }}</td>
                      
                      <td><a href="#" style="float:left;margin-right:10px" class="btn btn-default" onClick='triggerModal("{{$services->name}}","{{$services->id}}")'>Edit</a>
						{{ Form::open(array('method' => 'DELETE', 'route' =>array('services.destroy', $services->id))) }}
						{{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
						{{ Form::close() }}
					  </td>                    
                      
                      
                    </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
	 <!-- Modal -->
      <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              
			  
			  {{Form::open(array('url'=>'services','method'=>'PUT'))}}
				{{ Form::hidden('id',"$services->id", array('id'=>"serviceid")) }}
				{{ Form::text('name',Input::old("$services->name"), array('class'=>'form-control','placeholder'=>'Service name','id'=>"servicename"))}}
				<br>
				{{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
				{{Form::close()}}
            </div>
            <div class="modal-footer">
              
            </div>
          </div>
        </div>
      </div>
	  <script type="text/javascript">
		function triggerModal(avalue,idvalue) {
 
			document.getElementById('servicename').value = avalue;
			document.getElementById('serviceid').value = idvalue;
 
			$('#confirmModal').modal();
 
}
</script>
	  
@stop