@include('includes.header')
<div class="navbar navbar-inverse" role="navigation">
	@include('includes.top_header')
  </div>
  <div class="template-page-wrapper">
  {{ Form::open(array('url' => 'login' , 'class' => 'form-horizontal templatemo-signin-form' , 'role' => 'form', 'method' => 'post')) }}
      
        <div class="form-group">
          <div class="col-md-12">
		  <p>
			{{ $errors->first('username') }}
			{{ $errors->first('password') }}
		</p>
		  {{ Form::label('username', 'Username',array('class'=>'col-sm-2 control-label')) }}
           
            <div class="col-sm-10">
			{{ Form::text('username', Input::old('email'), array('placeholder' => 'Username','class'=>'form-control','id'=>'username')) }}
              
            </div>
          </div>              
        </div>
        <div class="form-group">
          <div class="col-md-12">
		  {{ Form::label('password', 'Password',array('class'=>'col-sm-2 control-label')) }}
            
            <div class="col-sm-10">
			{{ Form::password('password', array('placeholder' => 'Password','class'=>'form-control','id'=>'password')) }}
              
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <div class="col-sm-offset-2 col-sm-10">
              <div class="checkbox">
                <label>
                  <input type="checkbox"> Remember me
                </label>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <div class="col-sm-offset-2 col-sm-10">
              <input type="submit" value="Sign in" class="btn btn-default">
            </div>
          </div>
        </div>
      {{ Form::close() }}
    </div>
 
