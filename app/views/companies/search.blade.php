@extends('layouts.layout')

@section('content')
	<h1>Search for: {{Input::get('searchcomp')}}</h1>
	
	<div class="pull-right">{{$companylist->links()}}</div>
	<div class="table-responsive">
                <h4><a href="{{ URL::to('companies/create') }}" class="btn btn-default">Add New</a></h4>
                <table class="table table-striped table-hover table-bordered">
                  <thead>
                    <tr>
                    
                      <th>Company Name</th>
                      <th>Primary Services</th>
                      
					  <th>Seat Leasing</th>
                      <th>Fully Manage service</th>
                      
					  <th>Location</th>
                      <th>Australian <br> managed</th>
                      <th>Additional Features</th>
					  <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php //var_dump($companylist); exit();?>
					@foreach($companylist as $compInfo)
					<tr>
                     
                      <td>{{ $compInfo->name }}</td>
                      <td>
					  <ul class="searchlist">
					  @foreach ($compInfo->services as $service) 
						<li>{{ $service->name }}</li>
					  @endforeach
						</ul>
					  </td>
					
				<td>
				@foreach($compInfo->seatmodel as $seatmodel)
					@if($seatmodel->id == 1)
					{{ $seatmodel->pivot->priceRangeMin }} - {{ $seatmodel->pivot->priceRangeMax }}
						<ul class="searchlist">
						@foreach($compInfo->seatpackage as $seatpackage)
						
						@if($seatpackage->pivot->seatmodelID == 1)
							<li>{{$seatpackage->name}}</li>
						@endif
						
							
						@endforeach
						</ul>
					@endif
					@endforeach
				</td>
				<td>
				@foreach($compInfo->seatmodel as $seatmodel)
					@if($seatmodel->id == 2)
					{{ $seatmodel->pivot->priceRangeMin }} - {{ ($seatmodel->pivot->priceRangeMax==0)?"up":$seatmodel->pivot->priceRangeMax }}
						<ul class="searchlist">
						@foreach($compInfo->seatpackage as $seatpackage)
							@if($seatpackage->pivot->seatmodelID == 2)
								<li>{{$seatpackage->name}}</li>
							@endif
						@endforeach
						</ul>

					@endif
					@endforeach
				
				</td>
					  
					  <td><!--<a href="#" class="btn btn-default">Edit</a>-->
					  <ul class="searchlist">
					  @foreach ($compInfo->location as $location) 
						<li>{{ $location->name }}</li>
					  @endforeach
					  </ul>
					  
					  </td>                    
                      <td align="center">
					  @if($compInfo->aumanaged == 1)
					  <img src="{{ URL::asset('assets/images/aussiemanaged.png') }}" />
					  @else
					  <img src="{{ URL::asset('assets/images/noaussiemanaged.png') }}" />
					  
					  @endif
					  
                       
                      </td>
                      <td><!--<a href="#" class="btn btn-link">Delete</a>-->
					  <ul class="searchlist">
						@foreach ($compInfo->feature as $feature) 
							<li>{{ $feature->name }}</li>
						@endforeach
					  </ul>
					  </td>
					  
					  <td style="width: 145px;">
					  <a class="btn btn-small btn-info" href="{{ URL::to('companies/' . $compInfo->id . '/edit') }}">Edit</a>
					  
				{{ Form::open(array('url' => 'companies/' . $compInfo->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
				
				@if(!CompanyList::find($compInfo->id)->prioritylist)
				 <a style="margin-top:5px;font-size:12px" class="btn btn-small btn-info" href="{{ URL::to('priorityads/' . $compInfo->id ) }}">Add to Priority list</a>
				@endif
					  </td>
					  
                    </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
	 <script type="text/javascript">
$('ul.searchlist').each(function(){

  var LiN = $(this).find('li').length;

  if( LiN > 5){    
    $('li', this).eq(4).nextAll().hide().addClass('toggleable');
    $(this).append('<li class="more">Show all</li>');    
  }

});


$('ul.searchlist').on('click','.more', function(){

  if( $(this).hasClass('less') ){    
    $(this).text('Show all').removeClass('less');    
  }else{
    $(this).text('Show less').addClass('less'); 
  }

  $(this).siblings('li.toggleable').slideToggle();

});


</script>
	
@stop