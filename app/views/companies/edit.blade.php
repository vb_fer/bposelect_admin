@extends('layouts.layout')

@section('content')
	<h1>Edit {{$companyInfo->name}}</h1>
	{{ HTML::ul($errors->all() )}}
	{{ Form::model($companyInfo, array('route' => array('companies.update', $companyInfo->id), 'method' => 'PUT','files'=>'true')) }}
	<fieldset>
	<legend>Company Information</legend>
	<!--
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('stickysearch','Priority List',array('class'=>'control-label'))}}

	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::checkbox('stickysearch',1,$companyInfo->stickysearch, array('class'=>'form-controls'))}}
		{{ Form::label('stickysearch','Include in priority listing',array('class'=>'control-label'))}}
	</div>
	-->
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('name','Name',array('class'=>'control-label'))}}

	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('name',$companyInfo->name, array('class'=>'form-control'))}}
	</div>

	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('contact_person','Contact Person',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('contact_person',$companyInfo->contactperson, array('class'=>'form-control'))}}
	</div>
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('mobile','Mobile',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('mobile',$companyInfo->mobile, array('class'=>'form-control'))}}
	</div>
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('phone','Phone',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('phone',$companyInfo->phone, array('class'=>'form-control'))}}
	</div>
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('fax','Fax',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('fax',$companyInfo->fax, array('class'=>'form-control'))}}
	</div>
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('email','Email Address',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('email',null, array('class'=>'form-control'))}}
	</div>

	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('headoffice','Head Office',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('headoffice',null, array('class'=>'form-control'))}}
	</div>
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('Additional Addresses','Site Addresses',array('class'=>'control-label'))}}
	</div>
	
	<div class="col-md-9 margin-bottom-30">
		<a href="{{URL::to('companyaddresses/' . $companyInfo->id)}}">Add / Remove Addresses</a>
	</div>
	
	</fieldset>
	
	<fieldset>
	<legend>Internet and social media</legend>
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('website','WebSite',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('website',Input::old('website'), array('class'=>'form-control'))}}
	</div>
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('tw','Twitter',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('tw',Input::old('tw'), array('class'=>'form-control'))}}
	</div>
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('fb','Facebook',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('fb',Input::old('fb'), array('class'=>'form-control'))}}
	</div>
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('ln','Linkedin',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('ln',Input::old('ln'), array('class'=>'form-control'))}}
	</div>
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('gc','Google Circle',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('gc',Input::old('gc'), array('class'=>'form-control'))}}
	</div>
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('yt','Youtube',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('yt',Input::old('yt'), array('class'=>'form-control'))}}
	</div>
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('blog','Blog',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('blog',Input::old('blog'), array('class'=>'form-control'))}}
	</div>
	
	</fieldset>
	
	<fieldset>
	<legend>Company Description</legend>
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('filebanner','Company Banner',array('class'=>'control-label'))}}
	</div>
	
	<div class="col-md-9 margin-bottom-30">
		{{ Form::label('filebanner','upload company banner',array('class'=>'control-label'))}}
		{{ Form::file('filebanner','', array('class'=>'form-control'))}}
	</div>
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('shortdescription','Short Description',array('class'=>'control-label'))}}
		<br><span style="font-style:italic;">2 to 3 sentences</span>
	</div>
	
	<div class="col-md-9 margin-bottom-30">
		{{ Form::textarea('shortdescription',Input::old('shortdescription'), array('class'=>'form-control','rows'=>'3'))}}
	</div>
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('description','Description',array('class'=>'control-label'))}}
		
	</div>
	
	<div class="col-md-9 margin-bottom-30">
		{{ Form::textarea('description',Input::old('description'), array('class'=>'form-control','rows'=>'8'))}}
	</div>
	
	<script>
		CKEDITOR.replace('description');
	</script>
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('services','Primary Services',array('class'=>'control-label'))}}
	</div>
	
	<?php
		$services1 = array();
	foreach($companyInfo->services as $services){
		$services1[] = $services->id;
	}
	//print_r($services1);
	
	?>
	
	<div class="col-md-9 margin-bottom-30">
		{{ Form::select('services[]',Service::lists('name','id'),$services1 ,array('multiple'=>'true','class'=>'sel'))}}
	</div>
	
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('employeecount','Number of Employees',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::select('employeecount', array('0-100' => '0-100', 
											'101-200' => '101-200',
											'101-200' => '101-200',
											'201-300' => '201-300',
											'301-400' => '301-400',
											'401-500' => '401-500',
											'500 up' => '500 up'),Input::old('employeecount'),array('class'=>'form-control')) }}
	</div>
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('gallerycode','Gallery short code',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('gallerycode',$companyInfo->galleryshortcode, array('class'=>'form-control'))}}
	</div>
	
	</fieldset>
	
	<fieldset>
	
	<legend>Services</legend>
	
	<div class="col-md-3" style="margin-bottom:5px;">
		{{ Form::label('aumanaged','Is this an Australian managed company?',array('class'=>'control-label'))}}
	</div>
	
	<div class="col-md-9 margin-bottom-30">
		{{ Form::radio('aumanaged','1',($companyInfo->aumanaged == 1)?"true":"false", array('class'=>'form-control1','id'=>'auyes'))}} 
		{{ Form::label("auyes","Yes",array('class'=>'control-label'))}}
		{{ Form::radio('aumanaged','0',($companyInfo->aumanaged == 0)?"true":"false" ,array('class'=>'form-control1','id'=>'auno'))}} 
		{{ Form::label("auno","No",array('class'=>'control-label'))}}
	</div>
	
	
		<div class="col-md-3 margin-bottom-30">
		
		{{ Form::label('seatmodel','Seating Model',array('class'=>'control-label'))}} <br>
	</div>
	
	<div class="col-md-9 margin-bottom-30">
	
	
		@foreach($seatmodel as $key => $seatmodel)
		<?php $name = str_replace(' ','',$seatmodel->name);?>
		
		{{ Form::checkbox("seatmodel1[]","$seatmodel->id",$sm[$seatmodel->id],array('class'=>'form-controls chkseatmodel','id'=>"$name"))}}
		{{ Form::label("$name","$seatmodel->name",array('class'=>'control-label'))}}<br>
			<div style="margin:0 0 20px 20px;" id="div{{$name}}" class="div{{$seatmodel->id}}">
				<p style="margin-bottom:15px">
				{{ Form::label('Packages','Packages/Includes',array('class'=>'control-label'))}}
				{{ Form::select('package' . "$seatmodel->id" . '[]' ,SeatPackage::lists('name','id'),$package[$seatmodel->id] ,array('multiple'=>'true','class'=>'sel2'))}}
				</p>
				{{ Form::label('Minimum Price','Price Range',array('class'=>'control-label'))}}
				{{Form::select('pricerange' . $seatmodel->id,$budgets,$price[$seatmodel->id])}}
			</div>
		@endforeach	
	</div>

	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('location','Location',array('class'=>'control-label'))}}
	</div>
	
	<div class="col-md-9 margin-bottom-30">
	<?php
	foreach($companyInfo->location as $location){
		$location2[] = $location->id;	
	}
	
	$location2 = (empty($location2))?0:$location2;
	
	?>
	
		{{ Form::select('location[]',$locationList,$location2 ,array('multiple'=>'true','class'=>'sel'))}}
	</div>
	
	
		
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('features','Additional Features',array('class'=>'control-label'))}}
	
	</div>
	<?php
	foreach($companyInfo->feature as $feature){
		$feature2[] = $feature->id;	
	}
	
	$feature2 = (empty($feature2))?0:$feature2;
	
	?>
	<div class="col-md-9 margin-bottom-30">
			{{ Form::select('features[]',Feature::lists('name','id'), $feature2,array('multiple'=>'true','class'=>'sel'))}}
	</div>
	</fieldset>
	<div class="margin-bottom-30">
	 {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
	 </div>
	{{ Form::close() }}
	
	<script src="{{URL::asset('assets/js/jquery.multiple.select.js')}}"></script>
	<script>
		
	 $(document).ready(function(){
	$('.chkseatmodel').change(function () {
      $('#div'+$(this).attr('id')).slideToggle();
	  
    });
	
	$('input[name^=seatmodel]').each(function() {
	
			if($('#'+this.id).is(':checked')){
				$(".div" + this.value).css("display","block");
			}
			else{
				$(".div" + this.value).css("display","none");
			}
			//alert( this.id ); 
		});
	
	});

	</script>
	
    <script>
        $(".sel").multipleSelect({
            width: '100%',
            multiple: true,
            multipleWidth: 'auto',
			placeholder: 'Select from list',
			position: 'top'
        });
		
		$(".sel2").multipleSelect({
            width: '50%',
            multiple: true,
            multipleWidth: 'auto',
			placeholder: 'Select from list'
			
        });
    </script>
	
	
@stop