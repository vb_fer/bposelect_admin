@extends('layouts.layout')

@section('content')
<h1>Manage Addresses for {{$compname}}</h1>
<hr>
{{Form::open(array('url'=>'companyaddress'))}}
<div id="divaddresses" class="col-md-12">
			<div class="col-md-3 margin-bottom-15">
				{{ Form::label('otheraddress','Complete Address',array('class'=>'control-label'))}}
			</div>
			<div class="col-md-9 margin-bottom-15">
				{{ Form::text('otheraddress','', array('class'=>'form-control'))}}
			</div>
			
			
			<div class="col-md-2 margin-bottom-15">
				{{ Form::label('othercontact','Contact Person',array('class'=>'control-label'))}}
			</div>
			<div class="col-md-4 margin-bottom-15">
				{{ Form::text('othercontact','', array('class'=>'form-control'))}}
			</div>
			<div class="col-md-2 margin-bottom-15" style="padding:0">
				{{ Form::label('othercontactnumber','Contact Number',array('class'=>'control-label'))}}
			</div>
			<div class="col-md-4 margin-bottom-15">
				{{ Form::text('othercontactnumber','', array('class'=>'form-control'))}}
			</div>
			<div class="col-md-12 margin-bottom-15" style="text-align:center">
			<input type="hidden" value="{{$compid}}" name="compid">
			 {{ Form::submit('Add New Address', array('class' => 'btn btn-primary')) }}</div>
		</div>
{{Form::close()}}


<div class="table-responsive">
              
				
		<table class="table table-striped table-hover table-bordered">
                  <thead>
                    <tr>
                      <!--<th>#</th>-->
                      <th>Address</th>
					  <th>Contact Person</th>
					  <th>Contact Number</th>
					<th>action</th>
					
                    </tr>
                  </thead>
                  <tbody>
                    
					@foreach($compaddress as $address)
		
					<tr>
                      
                      <td>{{ $address->address }}</td>
					  <td> {{$address->othercontactperson}}	</td>
							
                      <td>{{$address->othercontactnumber}}</td>
                      <td><!--<a style="float:left;margin-right:10px" class="btn btn-default" href="{{ URL::to('location/' . $address->id . '/edit') }}" >Edit</a>-->
						
						{{ Form::open(array('method' => 'DELETE', 'route' =>array('companyaddress.destroy', $address->id))) }}
						{{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
						{{ Form::close() }}
					  </td>                    
                      
                      
                    </tr>
                  @endforeach
                  </tbody>
                </table>
		</div>

	
@stop