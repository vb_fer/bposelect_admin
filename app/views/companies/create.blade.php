@extends('layouts.layout')

@section('content')
	<h1>Add New</h1>
	{{ HTML::ul($errors->all() )}}
<div class="col-md-12">
	{{Form::open(array('url'=>'companies','files'=>true))}}
	<fieldset>
	<legend>Company Information</legend>
	<!--
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('stickysearch','Sticky search',array('class'=>'control-label'))}}
		
	</div>
	
	<div class="col-md-9 margin-bottom-30">
		{{Form::select('stickysearch',array('0'=>'No','1'=>'Yes'),'',array('class'=>'form-control'))}}
		
	</div>
	-->
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('name','Name',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('name',Input::old('name'), array('class'=>'form-control'))}}
	</div>
	

	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('contact_person','Contact Person',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('contact_person',Input::old('contact_person'), array('class'=>'form-control'))}}
	</div>
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('mobile','Mobile',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('mobile',Input::old('Mobile'), array('class'=>'form-control'))}}
	</div>
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('phone','Phone',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('phone',Input::old('Phone'), array('class'=>'form-control'))}}
	</div>
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('fax','Fax',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('fax',Input::old('Fax'), array('class'=>'form-control'))}}
	</div>
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('email','Email Address',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('email',Input::old('email'), array('class'=>'form-control'))}}
	</div>

	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('headoffice','Head Office',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('headoffice',Input::old('headoffice'), array('class'=>'form-control'))}}
	</div>
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('siteaddress','Site Addresses',array('class'=>'control-label'))}}
	</div>
	
	<div class="col-md-9 margin-bottom-30">
		<div id="divaddresses" class="col-md-12">
			<div class="col-md-3 margin-bottom-15">
				{{ Form::label('otheraddress1','Complete Address',array('class'=>'control-label'))}}
			</div>
			<div class="col-md-9 margin-bottom-15">
				{{ Form::text('otheraddress1','', array('class'=>'form-control'))}}
			</div>
			
			
			<div class="col-md-2 margin-bottom-15">
				{{ Form::label('othercontact1','Contact Person',array('class'=>'control-label'))}}
			</div>
			<div class="col-md-4 margin-bottom-15">
				{{ Form::text('othercontact1','', array('class'=>'form-control'))}}
			</div>
			<div class="col-md-2 margin-bottom-15" style="padding:0">
				{{ Form::label('othercontactnumber1','Contact Number',array('class'=>'control-label'))}}
			</div>
			<div class="col-md-4 margin-bottom-15">
				{{ Form::text('othercontactnumber1','', array('class'=>'form-control'))}}
			</div>
			<input type="hidden" id="addressctr" value="1" name="addressctr"/>
		</div>
		<div id="divbuttons" class="col-md-12 margin-bottom-15" style="text-align:center;margin-top:10px;">
		<input class="btn" type="button" id="add" value="add new"/>
		<input class="btn" type="button" id="removediv" value="Remove"/></div>
		
	</div>
	
	<script>
		$('#add').click(function (e) {
			
			var ctr = $("#addressctr").val();
		ctr++;
			fields = '<div id="div' + ctr +'" class="col-md-12" style="border:1px solid #ccc;padding:10px;margin-top:5px"><div class="col-md-3 margin-bottom-15">';
			fields += '<label for="otheraddress' + ctr +'" class="control-label">Complete Address</label></div>'
			fields += '<div class="col-md-9 margin-bottom-15">';
			fields += '<input class="form-control" name="otheraddress' + ctr+ '" type="text" value="" id="otheraddress1"></div>';
			fields += '<div class="col-md-2 margin-bottom-15">';
			fields += '<label for="othercontact' + ctr + '" class="control-label">Contact Person</label></div>';
			fields += '<div class="col-md-4 margin-bottom-15">';
			fields += '<input class="form-control" name="othercontact' + ctr + '" type="text" id="othercontact"></div>';
			fields += '<div class="col-md-2 margin-bottom-15" style="padding:0">';
			fields += '<label for="othercontactnumber' + ctr + '" class="control-label">Contact Number</label></div>';
			fields += '<div class="col-md-4 margin-bottom-15">';
			fields += '<input class="form-control" name="othercontactnumber' + ctr + '" type="text" id="othercontactnumber"></div>';
			fields += '</div>';
		
			$("#divaddresses").append(fields);
			$("#addressctr").attr("value",ctr);
		});
		
		$('#removediv').click(function (e) {
			e.preventDefault();
			ctr = $("#addressctr").val();
			if(ctr > 1){
			$("#div" + ctr).remove();
			$("#addressctr").attr("value",--ctr);
			}
		});
	
	
	</script>
	
	
	</fieldset>

	<fieldset>
	<legend>Internet and social media</legend>
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('website','WebSite',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('website',Input::old('website'), array('class'=>'form-control'))}}
	</div>
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('tw','Twitter',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('tw',Input::old('tw'), array('class'=>'form-control'))}}
	</div>
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('fb','Facebook',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('fb',Input::old('fb'), array('class'=>'form-control'))}}
	</div>
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('ln','Linkedin',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('ln',Input::old('ln'), array('class'=>'form-control'))}}
	</div>
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('gc','Google Circle',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('gc',Input::old('gc'), array('class'=>'form-control'))}}
	</div>
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('yt','Youtube',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('yt',Input::old('yt'), array('class'=>'form-control'))}}
	</div>
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('blog','Blog',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('blog',Input::old('blog'), array('class'=>'form-control'))}}
	</div>
	</fieldset>
	

	<fieldset>
	<legend>Company Description</legend>
	
	<div class="col-md-3 margin-bottom-15">
		{{ Form::label('filebanner','Company Banner',array('class'=>'control-label'))}}
		<p><i>recommended size: 696px x 131px</i></p>
	</div>
	
	<div class="col-md-9 margin-bottom-30">
		{{ Form::label('filebanner','upload company banner',array('class'=>'control-label'))}}
		{{ Form::file('filebanner','', array('class'=>'form-control'))}}
	</div>
	
	
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('shortdescription','Short Description',array('class'=>'control-label'))}}
		<br/><span style="font-style:italic;">2 to 3 sentences</span>
	</div>
	
	<div class="col-md-9 margin-bottom-30">
		{{ Form::textarea('shortdescription',Input::old('shortdescription'), array('class'=>'form-control','rows'=>'3'))}}
	</div>
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('description','Description',array('class'=>'control-label'))}}
		
	</div>
	
	<div class="col-md-9 margin-bottom-30">
		{{ Form::textarea('description',Input::old('description'), array('class'=>'form-control','rows'=>'8'))}}
	</div>
	
	<script>
		CKEDITOR.replace('description');
	</script>
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('services','Primary Services',array('class'=>'control-label'))}}
	</div>
	
	<div class="col-md-9 margin-bottom-30">
		{{ Form::select('services[]',Service::lists('name','id'),'' ,array('multiple'=>'true','class'=>'sel'))}}
	</div>
	
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('employeecount','Number of Employees',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::select('employeecount', array('0-100' => '0-100', 
											'101-200' => '101-200',
											'101-200' => '101-200',
											'201-300' => '201-300',
											'301-400' => '301-400',
											'401-500' => '401-500',
											'500 up' => '500 up'),Input::old('employeecount'),array('class'=>'form-control')) }}
	</div>
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('gallerycode','Gallery short code',array('class'=>'control-label'))}}
	</div>
	<div class="col-md-9 margin-bottom-30">
		{{ Form::text('gallerycode',Input::old('blog'), array('class'=>'form-control'))}}
	</div>
	
	</fieldset>
	
	<fieldset>
	
	<legend>Services</legend>
	
	<div class="col-md-3" style="margin-bottom:5px;">
		{{ Form::label('aumanaged','Is this an Australian managed company?',array('class'=>'control-label'))}}
	</div>
	
	<div class="col-md-9 margin-bottom-30">
		{{ Form::radio('aumanaged','1',true, array('class'=>'form-control1','id'=>'auyes'))}} 
		{{ Form::label("auyes","Yes",array('class'=>'control-label'))}}
		{{ Form::radio('aumanaged','0',false ,array('class'=>'form-control1','id'=>'auno'))}} 
		{{ Form::label("auno","No",array('class'=>'control-label'))}}
	</div>
	
	
	<div class="col-md-3 margin-bottom-30">
		
		{{ Form::label('seatmodel','Seating Model',array('class'=>'control-label'))}} <br>
	</div>
	
	<div class="col-md-9 margin-bottom-30">
	
		@foreach($seatmodel as $seatmodel)
		<?php $name = str_replace(' ','',$seatmodel->name);?>
		{{ Form::checkbox("seatmodel[]","$seatmodel->id", false ,array('class'=>'form-controls chkseatmodel','id'=>"$name"))}}
		{{ Form::label("$name","$seatmodel->name",array('class'=>'control-label'))}}<br>
			<div style="margin:0 0 20px 20px;" id="div{{$name}}" class="div{{$seatmodel->id}}">
				<p style="margin-bottom:15px">
				{{ Form::label('Packages','Packages/Includes',array('class'=>'control-label'))}}
				{{ Form::select('package' . "$seatmodel->id" . '[]' ,SeatPackage::orderBy("name")->lists('name','id'),'' ,array('multiple'=>'true','class'=>'sel2'))}}
				</p>
				{{ Form::label('Minimum Price','Price Range',array('class'=>'control-label'))}}
				{{Form::select('pricerange' . $seatmodel->id,$budgets,Input::old('pricerange'. $seatmodel->id))}}
			</div>
		
		@endforeach	
	</div>
	
	
	
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('location','Location',array('class'=>'control-label'))}}
	</div>
	
	<div class="col-md-9 margin-bottom-30">
		<?php
		
		
		?>
	
		{{ Form::select('location[]',$locationList,'' ,array('multiple'=>'true','class'=>'sel'))}}
	</div>
	
		
	<div class="col-md-3 margin-bottom-30">
		{{ Form::label('features','Additional Features',array('class'=>'control-label'))}}
	
	</div>
	
	<div class="col-md-9 margin-bottom-30">
			{{ Form::select('features[]',Feature::lists('name','id'),'' ,array('multiple'=>'true','class'=>'sel'))}}
	</div>
	</fieldset>
	<div class="margin-bottom-30">
	 {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
	 </div>
	{{Form::close()}}
</div>
	<script src="{{URL::asset('assets/js/jquery.multiple.select.js')}}"></script>
	<script>
		
	 $(document).ready(function(){
	$('.chkseatmodel').change(function () {
      $('#div'+$(this).attr('id')).slideToggle();
	  
    });
	
	$('input[name^=seatmodel]').each(function() {
	
			if($('#'+this.id).is(':checked')){
				$(".div" + this.value).css("display","block");
			}
			else{
				$(".div" + this.value).css("display","none");
			}
			//alert( this.id ); 
		});
	
	});

	</script>
	
    <script>
        $(".sel").multipleSelect({
            width: '100%',
            multiple: true,
            multipleWidth: 'auto',
			placeholder: 'Select from list',
			position: 'top'
			
        });
		
		$(".sel2").multipleSelect({
            width: '50%',
            multiple: true,
            multipleWidth: 'auto',
			placeholder: 'Select from list'
			
        });
    </script>
	
	
@stop
