@extends('layouts.layout')

@section('content')
	<h1>LOCATION</h1>
	<div class="col-md-6">
	  
				{{Form::open(array('url'=>'location'))}}
				
				<div class="margin-bottom-15">
				{{ Form::text('name',Input::old('name'), array('class'=>'form-control','placeholder'=>'Location'))}}
				</div>
				<?php 
				$locationlist = Location::lists('name','id');
				array_unshift($locationlist,"Parent");
				
				?>
				<div class="margin-bottom-15">
				{{Form::select('isparent',$locationlist,'0',array('class'=>'form-control'))}}
				</div>
				
				<div class="margin-bottom-15">
				{{ Form::textarea('locationdesc',Input::old('locationdesc'), array('class'=>'form-control','rows'=>'10'))}}
				</div>
				
				<div class="margin-bottom-15">
				{{ Form::text('gmapslatitude',Input::old('gmapslatitude'), array('class'=>'form-control','placeholder'=>'latitude'))}}
				</div>
				
				<div class="margin-bottom-15">
				{{ Form::text('gmapslongtitude',Input::old('gmapslongtitude'), array('class'=>'form-control','placeholder'=>'longtitude'))}}
				</div>
				
				{{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
				{{Form::close()}}
				
	</div>
	<div class="col-md-6">
	
	<div class="pull-right">{{$location->links()}}</div>
<div class="table-responsive">
              
				
		<table class="table table-striped table-hover table-bordered">
                  <thead>
                    <tr>
                      <!--<th>#</th>-->
                      <th>Location</th>
					  <th>Parent</th>
					<th>action</th>
					
                    </tr>
                  </thead>
                  <tbody>
                    
					@foreach($location as $location)
		
					<tr>
                      <!--<td>{{ $location->id }}</td>-->
                      <td>{{ $location->name }}</td>
					  <td> @if($location->isparent != 0)
								{{ $location->find($location->isparent)->name }}
								@endif
								</td>
							
                      
                      <td><a style="float:left;margin-right:10px" class="btn btn-default" href="{{ URL::to('location/' . $location->id . '/edit') }}" >Edit</a>
						
						{{ Form::open(array('method' => 'DELETE', 'route' =>array('location.destroy', $location->id))) }}
						{{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
						{{ Form::close() }}
					  </td>                    
                      
                      
                    </tr>
                  @endforeach
                  </tbody>
                </table>
		</div>
	</div>	
		 <!-- Modal -->
   
	  <script type="text/javascript">
		function triggerModal(avalue,idvalue) {
 
			document.getElementById('locationname').value = avalue;
			document.getElementById('locationid').value = idvalue;
 
			$('#confirmModal').modal();
 
}
</script>
<script>
		CKEDITOR.replace('locationdesc');
	</script>

@stop