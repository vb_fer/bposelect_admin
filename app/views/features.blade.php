@extends('layouts.layout')

@section('content')
	<h1>FEATURES</h1>
	<div class="table-responsive">
                <h4 class="pull-left">
				{{Form::open(array('url'=>'features'))}}
				{{ Form::text('name',Input::old('name'), array('class'=>'form-control','placeholder'=>'Feature'))}}
				{{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
				{{Form::close()}}
				</h4>
				<div class="pull-right">{{$features->links()}}</div>
		<table class="table table-striped table-hover table-bordered">
                  <thead>
                    <tr>
                      <!--<th>#</th>-->
                      <th>Feature</th>
					<th>action</th>
					
                    </tr>
                  </thead>
                  <tbody>
                    
					@foreach($features as $features)
					<tr>
                      <!--<td>{{ $features->id }}</td>-->
                      <td>{{ $features->name }}</td>
                      
                      <td><a href="#"  style="float:left;margin-right:10px" class="btn btn-default" onClick='triggerModal("{{$features->name}}","{{$features->id}}")'>Edit</a>
						
						{{ Form::open(array('method' => 'DELETE', 'route' =>array('feature.destroy', $features->id))) }}
						{{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
						{{ Form::close() }}
					  </td>                    
                      
                      
                    </tr>
                  @endforeach
                  </tbody>
                </table>
		</div>
		
		 <!-- Modal -->
      <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              
			  
			  {{Form::open(array('url'=>'features','method'=>'PUT'))}}
				{{ Form::hidden('id',"$features->id", array('id'=>"featuresid")) }}
				{{ Form::text('name',Input::old("$features->name"), array('class'=>'form-control','placeholder'=>'Service name','id'=>"featuresname"))}}
				<br>
				{{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
				{{Form::close()}}
            </div>
            <div class="modal-footer">
              
            </div>
          </div>
        </div>
      </div>
	  <script type="text/javascript">
		function triggerModal(avalue,idvalue) {
 
			document.getElementById('featuresname').value = avalue;
			document.getElementById('featuresid').value = idvalue;
 
			$('#confirmModal').modal();
 
}
</script>
@stop