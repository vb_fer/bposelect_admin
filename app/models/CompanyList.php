<?php

class CompanyList extends Eloquent {

	protected $table = 'companylist';
	
	public function seatmodel(){
		return $this->belongsToMany('SeatModel','SeatModel_Comp','companyID','seatModelID')->withPivot('priceRangeMin','priceRangeMax');
	}
	
	public function location(){
		return $this->belongsToMany('Location','location_company','companyID','locationID');
	}
	
	public function services(){
		return $this->belongsToMany('Service','services_company','companyID','servicesID');
	}
	
	public function feature(){
		return $this->belongsToMany('Feature','feature_company','companyID','featureID');
	}
	
	public function seatpackage(){
		return $this->belongsToMany('SeatPackage','SeatModel_Package','companyID','packageID')->withPivot('seatmodelID')->orderby("Seat_Package.name");
	}
	
	public function addresscompany(){
		return $this->hasMany('AddressCompany','companyID');
	}
	
	public function prioritylist(){
		return $this->hasOne('PriorityList','companyID');
	}
	
	}
