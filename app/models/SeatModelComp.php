<?php

class SeatModelComp extends Eloquent {

	protected $table = 'seatmodel_comp';
	
	public function seat_package() {
			return $this->belongsToMany('SeatPackage','seatmodel_package','seatModel_compID','packageID');
	}
	
	}