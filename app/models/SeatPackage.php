<?php

class SeatPackage extends Eloquent {

	protected $table = 'Seat_Package';
	
	public function companylist()
	{
		return $this->belongsToMany('CompanyList','SeatModel_Package','packageID','companyID')->withPivot('seatmodelID')->orderby("Seat_Package.name");
	}
	
	public function seatmodel_comp()
	{
	//	return $this->belongsToMany('SeatModel_Comp','SeatModel_Package','packageID','seatModel_compID');
	}
	
	

}